[![pipeline status](https://gitlab.com/fathinah/delicae/badges/master/pipeline.svg)](https://gitlab.com/fathinah/delicae/-/commits/master)
[![coverage report](https://gitlab.com/fathinah/delicae/badges/master/coverage.svg)](https://gitlab.com/fathinah/delicae/-/commits/master)

# Delicae
Kelompok: 5

Nama-nama anggota kelompok:
1. Fathinah Asma Izzati
2. M. Rasyid
3. M. Rifqi
4. Nabila Ayu Dewanti

## Deskripsi:
Delicae adalah platform akselerasi bisnis UMKM Indonesia.